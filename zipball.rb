require 'rubygems'
require 'zip'

folder = "/Users/sean/Projects/test"
input_filenames = ['中文.txt', 'test.txt', 'test.rb']

zipfile_name = "/Users/sean/Projects/test.zip"

Zip::File.open(zipfile_name, Zip::File::CREATE) do |zipfile|
  input_filenames.each do |filename|
    # Two arguments:
    # - The name of the file as it will appear in the archive
    # - The original file, including the path to find it
    zipfile.add(filename.encode('gb2312'), folder + '/' + filename)
  end
  zipfile.get_output_stream("myFile") { |os| os.write "myFile contains just this" }
end
