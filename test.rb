class Foo
  def bar
    'Hello'
  end
end

class Foo
  def bar:after
    super + ' World'
  end
end
